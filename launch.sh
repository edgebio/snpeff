#!bin/bash
#
# Name:    snpEff Plugin
#
# Purpose: Annotates variants based on the snpEff open source program
#
# Author:   David Jenkins, EdgeBio Systems
#          
# Date:     Created 20111206
# History:  20120115 - Added support for unified variant caller
#           20120207 - Minor Bug Fixes and Update to snpEff 2.0.5d
#                      Dynamically creates snpEff.config in out directory
#                          that corresponds to the install dir of the plugin
#
# Copyright (C) 2011 Edge BioSystems, Inc.  All Rights Reserved
############################################################################
VERSION="3.0.1"
#autorundisable

# Functions from Ion Torrent plugins, modified by dfj3
print_html_head ()
{
local JS;
local CSS;
echo "    <head>
        <script type=\"text/javascript\" src=\"/site_media/jquery/js/jquery-1.6.1.min.js\"></script>
        <script type=\"text/javascript\" src=\"/site_media/jquery/js/jquery-ui-1.8.13.min.js\"></script>
<script type=\"text/javascript\" src=\"/site_media/jquery/js/tipTipX/jquery.tipTipX.js\"></script>";

for JS in `ls -1 ${RESULTS_DIR}/js/*.js`
do
    JS=`echo ${JS} | sed -e 's_.*/__g'`;
    echo \
	"<script type=\"text/javascript\" src=\"./js/${JS}\"></script>";
done

echo \
"        <link rel=\"stylesheet\" type=\"text/css\" href=\"/site_media/stylesheet.css\"/>
        <link type=\"text/css\" href=\"/site_media/jquery/css/aristo/jquery-ui-1.8.7.custom.css\" rel=\"stylesheet\" />
        <link href=\"/site_media/jquery/js/tipTipX/jquery.tipTipX.css\" rel=\"stylesheet\" type=\"text/css\" />
        <link rel=\"stylesheet\" type=\"text/css\" href=\"/site_media/report.css\" media=\"screen\" />";
for CSS in `ls -1 ${RESULTS_DIR}/css/*.css`
do
    CSS=`echo ${CSS} | sed -e 's_.*/__g'`;
    echo \
	"<link rel=\"stylesheet\" type=\"text/css\" href=\"./css/${CSS}\" />";
done

echo \
    "<script type=\"text/javascript\">
\$(function() {
\$().tipTipDefaults({ delay : 0 });
\$(\".tip\").tipTip({ position : \"bottom\" });
});
</script>
<script type=\"text/javascript\">
\$(function() {
\$( \"#accordion\").accordion();
});
</script>
    </head>";
}

print_html_end_javascript ()
{
echo "            <!-- zebra stripe the tables -->
<script type=\"text/javascript\">
 
 \$(document).ready(function(){

   \$('h2').append('<a href=\"javascript:;\" class=\"expandCollapseButton\" title=\"Collapse Section\"></a>');

\$('.expandCollapseButton').toggle(function() {
if ( \$(this).attr('title') == 'Collapse Section'){
\$(this).css('background-position','right top');
\$(this).attr('title','Expand Section');
}else{
\$(this).css('background-position','left top');
\$(this).attr('title','Collapse Section');
}
}, function() {
if ( \$(this).attr('title') == 'Expand Section'){
\$(this).css('background-position','left top');
\$(this).attr('title','Collapse Section');
}else{
\$(this).css('background-position','right top');
\$(this).attr('title','Expand Section');
}
});

\$('.expandCollapseButton').click(function(event){
\$(event.target).parent().parent().toggleClass('small');
\$(event.target).parent().next().slideToggle();
});

\$('#tf .expandCollapseButton').css('background-position','right top');\$('#tf .expandCollapseButton').attr('title','Expand Section');\$('#tf').parent().toggleClass('small');\$('#tf').next().toggle();
//start overlay

\$(\".heading tbody tr\").mouseover(
function(){
\$(this).addClass(\"table_hover\");

}).mouseout(
function(){
\$(this).removeClass(\"table_hover\");
});

\$(\".noheading tbody tr\").mouseover(
function(){
\$(this).addClass(\"table_hover\");

}).mouseout(
function(){
\$(this).removeClass(\"table_hover\");
});

\$(\".heading tr:odd\").addClass(\"zebra\");
\$(\".noheading tr:odd\").addClass(\"zebra\");

\$('#datatable').dataTable( {";
echo "\"aLengthMenu\": [[0], [0]],";
echo "\"sDom\": '<\"top\"fl>rt<\"bottom\"ip><\"spacer\">', 
\"sScrollX\": \"100%\",
\"sScrollXInner\": \"110%\",
\"bScrollCollapse\": true,
\"sPaginationType\": \"full_numbers\"
} );
});
 
  </script>
";
}

#*! @function
#  @param $1 FilePath to original zipped vcf
#  @param $2 Short Name of Plugin
#  @param $3 Long Name of Plugin
#  @param $4 snpEff Reference Library
#  @param $5 unique output identifier
#  @param $6 location of unzipped vcf in the plugin directory
run_snpeff_analysis ()
{
  local VCF_LOCATION="$1"
  local PLUGIN_SHORTNAME="$2"
  local PLUGIN_LONGNAME="$3"
  local REFERENCE="$4"
  local UNIQUEIDENTIFIER="$5"
  local VCF_FILEPATH="$6"

  #gunzip the file and put it in the snpeff out directory
  echo "Running plugin on ${VCF_LOCATION}" >&2
  ${DIRNAME}/bgzip -c -d ${VCF_LOCATION} > ${VCF_FILEPATH}

  #sort the vcf (issue with VC 3.0)
  REFERENCE_PRE=${TSP_FILEPATH_GENOME_FASTA%.*}
  ${DIRNAME}/scripts/vcfsorter.pl ${REFERENCE_PRE}.dict ${VCF_FILEPATH} > ${VCF_FILEPATH}_sorted
  mv -f ${VCF_FILEPATH}_sorted ${VCF_FILEPATH}

  #grab the complete status of the referenced variantCaller plugin
  PLUGINCOMPLETE=`curl --silent --header ${api_header} --location ${plugin_url} | jsonpipe | grep "pluginState/${PLUGIN_SHORTNAME}"`
  
  #Check plugin is in complete state
  if [[ ${PLUGINCOMPLETE} =~ "Completed" ]]; then
      #Run snpeff
      java -jar ${DIRNAME}/snpEff.jar eff -v -c ${RESULTS_DIR}/snpEff.config -i vcf -o vcf ${REFERENCE} ${VCF_FILEPATH} > ${RESULTS_DIR}/${UNIQUEIDENTIFIER}_variants_snpeff.vcf
      
      #bgzip and tabix outputs
      ${DIRNAME}/bgzip ${RESULTS_DIR}/${UNIQUEIDENTIFIER}_variants_snpeff.vcf
      ${DIRNAME}/bgzip ${VCF_FILEPATH}
      ${DIRNAME}/tabix -p vcf ${RESULTS_DIR}/${UNIQUEIDENTIFIER}_variants_snpeff.vcf.gz
      ${DIRNAME}/tabix -p vcf ${VCF_FILEPATH}.gz
      
      #rename outputs specific to this plugin with the short name
      mv ${RESULTS_DIR}/snpEff_summary.html ${RESULTS_DIR}/${UNIQUEIDENTIFIER}_summary.html
      mv ${RESULTS_DIR}/snpEff_genes.txt ${RESULTS_DIR}/${UNIQUEIDENTIFIER}_genes.txt
      sed -i "s/snpEff_genes.txt/${UNIQUEIDENTIFIER}_genes.txt/g" ${RESULTS_DIR}/${UNIQUEIDENTIFIER}_summary.html

      #add plugin results to block report	
	echo "<tr><td><a href='${UNIQUEIDENTIFIER}_variants_snpeff.vcf.gz'>${PLUGIN_LONGNAME} Annotated Variants (VCF) </a> </td> </tr>
<tr><td><a href='${UNIQUEIDENTIFIER}_variants_snpeff.vcf.gz.tbi'>${PLUGIN_LONGNAME} Annotated Variants Index (TBI) </a> </td> </tr>" >> "${RESULTS_DIR}/${PLUGINNAME}_summary_block.html";
else
    echo "${PLUGIN_LONGNAME} not in Complete state." >&2
    echo "snpEff will not be run on this plugin's vcf file" >&2
    echo "Run the plugin again when all desired plugins are in Completed State" >&2
    rm -f ${RESULTS_DIR}/${UNIQUEIDENTIFIER}_variants_snpeff.vcf
fi
}

#Set the URL for the API requests
plugin_url=http://127.0.0.1/rundb/api/v1/results/${RUNINFO__PK}/plugin/
#Set the HTTP Headers
api_header="Content-Type: application/json;Accept: application/json"

#Remove files if re-run
if [ -d ${RESULTS_DIR}/css ]; then
    echo "Re-running plugin, removing files from previous run" >&2
    rm -fr ${RESULTS_DIR}/*.vcf.gz
    rm -f ${RESULTS_DIR}/*.vcf.gz.tbi
    rm -f ${RESULTS_DIR}/*_summary.html
    rm -f ${RESULTS_DIR}/*_block.html
    rm -f ${RESULTS_DIR}/*_genes.txt
    rm -rf ${RESULTS_DIR}/css
    rm -rf ${RESULTS_DIR}/js
fi

#Only hg19 support is implemented. #TODO: Implement downloading and using new databases with instance.html
CHOSENGENOME="hg19"

#Print out genome name to stdout
echo "Genome used: ${TSP_FILEPATH_GENOME_FASTA}" >&2
if [[ ${TSP_FILEPATH_GENOME_FASTA} =~ "${CHOSENGENOME}" ]]; then
    echo "${CHOSENGENOME} used. Running snpEff" >&2
else
    echo "ERROR: ${CHOSENGENOME} not used.  Other snpEff databases not implemented" >&2
    exit 1;
fi

#make the _block.html file with annotated vcf links
###  block run ###
mkdir -vp ${RESULTS_DIR}/js; 
cp -v ${DIRNAME}/js/*.js ${RESULTS_DIR}/js/;
mkdir -vp ${RESULTS_DIR}/css;
cp -v ${DIRNAME}/css/*.css ${RESULTS_DIR}/css/;

echo -n '' > "${RESULTS_DIR}/${PLUGINNAME}_summary_block.html";
echo '<html>' >> "${RESULTS_DIR}/${PLUGINNAME}_summary_block.html";
print_html_head >> "${RESULTS_DIR}/${PLUGINNAME}_summary_block.html";
echo -e '\t<body>' >> "${RESULTS_DIR}/${PLUGINNAME}_summary_block.html";
echo " <div id=\"snpEff\" name=\"snpEff\" class=\"report_block\">
<h2>snpEff Summary</h2>
                <div>
                    <br/>
<table class=\"noheading\">" >> "${RESULTS_DIR}/${PLUGINNAME}_summary_block.html";

#configure snpEff.config file
sed "s#data_dir = .\+#data_dir = ${DIRNAME}/data/#g" ${DIRNAME}/snpEff.config > ${RESULTS_DIR}/snpEff.config

#Check for barcodes
if [ -f ${TSP_FILEPATH_BARCODE_TXT} ]; then
    echo "Barcodes detected" >&2
    for BARCODE_NAME in `cat ${TSP_FILEPATH_BARCODE_TXT} | grep "^barcode" | cut -f 2 -d ,`
    do
	if [ -e ${RESULTS_DIR}/../variantCaller_out/${BARCODE_NAME}/TSVC_variants.vcf.gz ]; then
	    run_snpeff_analysis "${RESULTS_DIR}/../variantCaller_out/${BARCODE_NAME}/TSVC_variants.vcf.gz" "variantCaller" "${BARCODE_NAME}" "${CHOSENGENOME}" "variantCaller_${BARCODE_NAME}" "${RESULTS_DIR}/${BARCODE_NAME}_variants.vcf"
	fi
    done
else
    #Call on vcf from variantCaller
    if [ -e ${RESULTS_DIR}/../variantCaller_out/TSVC_variants.vcf.gz ]; then
	run_snpeff_analysis "${RESULTS_DIR}/../variantCaller_out/TSVC_variants.vcf.gz" "variantCaller" "Variant Caller" "${CHOSENGENOME}" "variantCaller" "${RESULTS_DIR}/TSVC_variants.vcf";
    fi
fi

#End the block html file
echo "</div></div>" >> "${RESULTS_DIR}/${PLUGINNAME}_summary_block.html";
print_html_end_javascript >> "${RESULTS_DIR}/${PLUGINNAME}_summary_block.html";
echo -n '</body></html>' >> "${RESULTS_DIR}/${PLUGINNAME}_summary_block.html";
