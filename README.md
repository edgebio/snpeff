snpEff
======

Installation
------------

To install this plugin on your Torrent Server, navigate to the snpEff page on the Ion Torrent Community [Torrent Browser Plugin Store](http://lifetech-it.hosted.jivesoftware.com/docs/DOC-2594 "Plugin: snpEff").  That installation includes the binary database for hg19 in /snpEff/data/hg19 which is not included in this repository

Features in Development
-----------------------

### Barcode Support

Many people have requested barcode support for this plugin.  This should be a straightforward addition to the plugin for hg19.

### Additional References

Currently the plugin only supports experiments aligned to hg19.  There are many more databases available on the snpEff website.  Ion Torrent provides support for an 'uploader' function which I think we could incorporate into this plugin to allow end users to upload a reference binary, and select the desired reference database from a drop down list when running the plugin.

About the Plugin
----------------

A variant annotation and effect prediction tool.

This plugin is a wrapper around [snpEff](http://snpeff.sourceforge.net/) for use with the hg19 reference. 
Plugin Author: [David Jenkins - EdgeBio](mailto:djenkins@edgebio.com) 
snpEff Author: [Pablo Cingolani](mailto:pcingola@users.sourceforge.net)
